export default class Level {
    app;
    list = [];

    n;
    bgColor;
    foesNb;

    constructor(app) {
        this.app = app;
        this.initList();
    }

    goNext = () => {
        this.init(this.n + 1);
    }

    init = (n) => {
        let level = this.list[n];
        this.n = n;
        this.bgColor = level.bgColor;
        this.foesNb = level.foesNb;
    }

    initList = () => {
        this.list = [
            {
                bgColor: '#2395e6',//Blue
                foesNb: 3,
            },
            {
                bgColor: '#00b773',//Green blue
                foesNb: 6,
            },
            {
                bgColor: '#7ea720',//Green yellow
                foesNb: 9,
            },
            {
                bgColor: '#d06000',//Brown
                foesNb: 12,
            },
            {
                bgColor: '#d41a56',//Red pink
                foesNb: 15,
            },
        ];
    }
}