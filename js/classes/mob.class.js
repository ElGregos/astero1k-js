export default class Mob {
    x = 0;
    y = 0;
    radius = 0;
    angle = 0;
    speed = 0;
    type = 0;//1=plyr, 2=foe, 4=bullet.

    //For player and bullets.
    nrg;
    shooterX;
    shooterY;
    collide;

    constructor(x, y, radius, angle, speed, type) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.angle = angle;
        this.speed = speed;
        this.type = type;
        this.nrg = 1;//Barely alive.
    }

    collideWith = mob => {
        if (
            this === mob
            || this.type === mob.type
            || this.type + mob.type === 5
        ) {
            return false;
        }
        let dist = Math.sqrt((this.x - mob.x) ** 2 + (this.y - mob.y) ** 2);
        return (this.radius + mob.radius > dist);
    }
}