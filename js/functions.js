export default class ThoseFuncs {

    //Returns middle of 3 values.
    mid = (a, b, c) => {
        if (a < b) {
            return (b < c) ? b : ((a < c) ? c : a)
        } else {
            return (a < c) ? a : ((b < c) ? c : b)
        }
    }

    //Modulo (% is a remainder, unusable with negative values).
    mod = (a, n) => {
        let m = ((a % n) + n) % n
        return m
    }

    //Returns a random number >=0 and <n.
    rnd = n => Math.random() * n;
}
