export default class setup {
    canvas = document.getElementById('canvas');
    screen = { width: 128, height: 128 };
    // screen = { width: 450, height: 300 };
    controls = {
        left: { codes: ['ArrowLeft', 'KeyA'] },
        right: { codes: ['ArrowRight', 'KeyD'] },
        action: { codes: ['Space', 'KeyZ', 'KeyC', 'KeyN'] },
        help: { codes: ['Enter'] },
    };
}