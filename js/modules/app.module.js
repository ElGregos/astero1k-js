import ControlsModule from './controls.module.js';
import Level from '../classes/level.class.js';
import ViewModule from './view.module.js';

import F from '../functions.js';
const f = new F();

import Mob from '../classes/mob.class.js'
export default class App {
    controlsModule = new ControlsModule(this);
    viewModule = new ViewModule(this);

    states = {
        game: {
            init: () => this.gameInit(),
            update: () => this.gameUpdate(),
            draw: () => this.viewModule.gameDraw()
        },
        title: {
            update: () => this.titleUpdate(),
            draw: () => this.viewModule.titleDraw()
        },
        victory: {
            update: () => this.victoryUpdate(),
            draw: () => this.viewModule.victoryDraw()
        },
    };
    state = {};
    draw;
    update;

    time = 0;
    timeMs = 0;

    mobs = [];
    plyr;

    constructor() {
        this.init();
    }

    init = () => {
        this.setState('title');
        window.requestAnimationFrame(this.loop);
    }

    gameInit = () => {
        this.level = new Level(this);
        this.level.init(0);
        this.levelInit();
    }

    gameUpdate = () => {
        if (this.mobs.length <= 1) {
            this.levelNext();
            return;
        }

        //Player controls.
        if (this.controlsModule.isDownOnce('help')) {
            this.viewModule.showHelp = !this.viewModule.showHelp;
        }
        if (this.controlsModule.isDown('left')) {
            this.plyr.angle -= .08;
        }
        if (this.controlsModule.isDown('right')) {
            this.plyr.angle += .08;
        }
        if (this.controlsModule.isDown('action')) {
            this.shoot();
        }

        //Mobs update.
        this.plyr.collide = false;
        this.mobs.forEach(mob => {
            let deltaX = Math.cos(mob.angle) * mob.speed,
                deltaY = Math.sin(mob.angle) * mob.speed;
            deltaX *= this.delay;
            deltaY *= this.delay;
            mob.x = f.mod(mob.x + deltaX, 128);
            mob.y = f.mod(mob.y + deltaY, 128);

            //Plyr collision?
            if (this.plyr.collideWith(mob)) {
                this.plyr.collide = true;
            }
            //Bullet?
            if (mob.type === 4) {
                this.mobs.forEach(mob2 => {
                    //Collision with foe and bullet didn't collide yet?
                    if (mob2.type === 2 && !mob.collide) {
                        if (mob.collideWith(mob2)) {
                            mob.collide = true;
                            //If foe's big enough, split it.
                            if (mob2.radius > 2) {
                                //Recycling the bullet into a new foe...
                                mob.type = 2;
                                mob.radius = mob2.radius * .4;
                                mob.speed = mob2.speed;
                                //...if it's also big enough.
                                if (mob.radius < 1) {
                                    mob.nrg = -1;
                                }
                                mob2.radius *= .6;
                            } else {
                                mob.nrg = -1;
                                mob2.nrg = -1;
                            }
                        }
                    }
                })
                if (!mob.collide) {
                    mob.nrg -= 1;
                }
            }
        })

        //Player update.
        this.plyr.shooterX = this.plyr.x + Math.cos(this.plyr.angle + Math.PI) * 5;
        this.plyr.shooterY = this.plyr.y + Math.sin(this.plyr.angle + Math.PI) * 5;
        this.plyr.speed *= .98;

        if (this.plyr.collide) {
            // this.plyr.nrg = Math.max(0, this.plyr.nrg - 1);
            this.plyr.nrg -= 1;
            if (this.plyr.nrg < 0) {
                this.levelInit();
                return;
            }
        }
        this.plyr.nrg = Math.min(100, this.plyr.nrg + .25);

        //Cleaning dead mobs (with nrg<0)
        let mobs = [];
        this.mobs.forEach(mob => {
            if (!(mob.nrg < 0)) {
                mobs.push(mob);
            }
        })
        this.mobs = mobs;
    }

    levelInit = () => {
        this.mobs = [];
        this.plyr = new Mob(64, 64, 3, 0, 0, 1);
        this.plyr.nrg = 100;
        this.mobs.push(this.plyr);
        for (let mobN = 0; mobN < this.level.foesNb; mobN++) {
            let speed = f.rnd(.25) + .0625;
            let mob = new Mob(f.rnd(128), f.rnd(128), f.rnd(5) + 2, f.rnd(Math.PI * 2), speed, 2);
            this.mobs.push(mob);
        }
    }

    levelNext = () => {
        if (this.level.n + 1 < this.level.list.length) {
            this.level.goNext();
            this.levelInit();
        } else {
            this.setState('victory');
        }
    }

    loop = (timeMs) => {
        window.requestAnimationFrame(this.loop);
        let time = timeMs / (1000 / 60);
        this.delay = time - this.time;
        this.time = time;
        this.timeMs = timeMs;
        this.update();
        this.draw();
    }

    setState = state => {
        this.startTime = this.time;
        this.state = this.states[state];
        if (this.state.init) this.state.init();
        if (this.state.update) this.update = this.state.update;
        if (this.state.draw) this.draw = this.state.draw;
    }

    shoot = () => {
        if (this.plyr.nrg >= 6) {
            this.plyr.speed = Math.min(2, this.plyr.speed + .06);
            this.plyr.nrg = Math.max(0, this.plyr.nrg - .5);
            if (this.time % 3 <= 1) {
                //New bullet.
                let bullet = new Mob(this.plyr.shooterX, this.plyr.shooterY, 1, this.plyr.angle + Math.PI, 1, 4);
                bullet.nrg = 100;
                this.mobs.push(bullet);
            }
        }
    }

    titleUpdate = () => {
        if (this.controlsModule.isDownOnce('action')) {
            this.setState('game');
        }
    }

    victoryUpdate = () => {
        if (this.controlsModule.isDownOnce('action')) {
            this.setState('title');
        }
    }
}