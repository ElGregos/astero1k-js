import F from '../functions.js';
const f = new F();
import Setup from '../setup.js';
const setup = new Setup();

export default class View {

    app;
    canvas = setup.canvas;
    ctx = this.canvas.getContext('2d');
    showHelp = false;
    zoom = 0;

    //METHODS
    constructor(app) {
        this.init(app);
    }

    circ = (x, y, r, c, thick) => {
        this.ctx.strokeStyle = c;
        this.ctx.lineWidth = thick * this.zoom;
        this.ctx.beginPath();
        this.ctx.arc(x * this.zoom, y * this.zoom, r * this.zoom, 0, Math.PI * 2);
        this.ctx.stroke();
    }

    circfill = (x, y, r, c) => {
        this.ctx.fillStyle = c;
        this.ctx.beginPath();
        this.ctx.arc(x * this.zoom, y * this.zoom, r * this.zoom, 0, Math.PI * 2);
        this.ctx.fill();
    }

    cls = c => {
        this.rectfill(0, 0, setup.screen.width, setup.screen.height, c);
    }

    gameDraw = () => {
        let bgCol = this.app.level.bgColor;
        this.cls(bgCol);
        //Drawing mobs.
        for (let mobN = this.app.mobs.length - 1; mobN >= 0; mobN--) {
            let mob = this.app.mobs[mobN];
            if (mob.type === 2) {
                //Draw foes
                this.circfill(mob.x, mob.y, mob.radius + .8, bgCol);
                this.circfill(mob.x, mob.y, mob.radius, '#000000');
            } else if (mob.type === 4) {
                //Draw bullets
                this.circfill(mob.x, mob.y, Math.min(mob.radius, mob.nrg / 10), '#ffdd00');
            } else {
                //Draw plyr.
                this.circfill(mob.shooterX, mob.shooterY, 2, '#ffdd00');
                if (this.app.plyr.collide) {
                    this.circ(mob.x, mob.y, mob.radius, '#ffffff', .8);
                } else {
                    this.circfill(mob.x, mob.y, mob.radius, '#ffffff');
                }
            }
        }
        //Health bar.
        this.ctx.lineJoin = 'round';
        this.rectfill(10, 10, 108 * this.app.plyr.nrg / 100, 5, bgCol + '60', .8);
        this.rect(10, 10, 108 * this.app.plyr.nrg / 100, 5, '#ffffff', .8);

        //Stats
        if (this.showHelp) {
            let statW = 3;
            this.print('delay: ' + Math.round(this.app.delay * 100) / 100, 0, 0, '#ffffff', statW);
            this.print('mobs: ' + this.app.mobs.length, 0, statW, '#ffffff', statW);
        }
    }

    init = app => {
        this.app = app;
        this.resize();
        window.addEventListener('resize', this.resize);
    }

    print = (str, x, y, c, size) => {
        this.ctx.font = ((size || 5) * this.zoom) + 'px sans-serif';
        this.ctx.fillStyle = c;
        this.ctx.textBaseline = 'top';
        this.ctx.fillText(str, x * this.zoom, y * this.zoom);
    }

    rect = (x, y, w, h, c, thick) => {
        this.ctx.strokeStyle = c;
        this.ctx.lineWidth = thick * this.zoom;
        this.ctx.strokeRect(x * this.zoom, y * this.zoom, w * this.zoom, h * this.zoom);
    }

    rectfill = (x, y, w, h, c) => {
        this.ctx.fillStyle = c;
        this.ctx.fillRect(x * this.zoom, y * this.zoom, w * this.zoom, h * this.zoom);
    }

    resize = () => {
        this.canvas.height = 0;//To prevent scrollbar to mess with screen width.
        let hRatio = this.canvas.parentElement.clientWidth / setup.screen.width,
            vRatio = this.canvas.parentElement.clientHeight / setup.screen.height;
        this.zoom = Math.min(hRatio, vRatio);
        this.canvas.width = setup.screen.width * this.zoom;
        this.canvas.height = setup.screen.height * this.zoom;
        this.canvas.style.marginTop = (this.canvas.parentElement.clientHeight - setup.screen.height * this.zoom) / 2;
    }

    titleDraw = () => {
        this.cls('#8da0a5');
        this.print('ASTERO1K', 10, 40, '#5f444e', 20);
        this.print('A minimal ES6 game', 20, 60, '#5f444e', 9);
        this.print('Left & Right, Space for action', 20, 105, '#5f444e', 6.5);
    }

    victoryDraw = () => {
        this.cls('#8da0a5');
        this.print('CONGRATS!', 6, 40, '#5f444e', 20);
        this.print('A minimal victory screen', 15, 60, '#5f444e', 9);
        this.print('Space to restart', 20, 105, '#5f444e', 6.5);
    }
}